import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="biotracker-sleap",
    version="0.0.1",
    author="Max Breitenfeldt",
    author_email="pip@mxbr.me",
    description="Python adapter to enable usage of SLEAP from biotracker",
    long_description=long_description,
    long_description_content_type="text/markdown",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: Linux",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.6",
    # dependency_links=[
        # 'git+ssh://git@git.imp.fu-berlin.de/swp21_biotracker/sleap.git@develop',
    # ],
    install_requires=[
        'sleap @ git+ssh://git@git.imp.fu-berlin.de/swp21_biotracker/sleap.git@develop#egg=sleap',
    ]
)
