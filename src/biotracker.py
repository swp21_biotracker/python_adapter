import sleap
import math
from sleap.nn.inference import load_model
from multiprocessing import shared_memory
import numpy as np
import time
import zmq

class SharedArray():
    def __init__(self, shape, dtype=np.float32, nbytes=np.float32(0).nbytes):
        self.shape = shape
        self.dtype = dtype
        self.nbytes = nbytes

    def __enter__(self):
        size = np.prod(self.shape) * self.nbytes
        self.shm = shared_memory.SharedMemory(create=True, size=size)
        self.ndarray = np.ndarray(self.shape, self.dtype, buffer=self.shm.buf)
        return (self.shm.name, self.ndarray)

    def __exit__(self, type, value, traceback):
        self.shm.close()
        self.shm.unlink()

class BiotrackerAdapter():
    def __init__(self, model_path, verbose=False):
        self.model = BottomUpPredictorWrapper(model_path)
        self.zmq_context = zmq.Context()
        self.socket = self.zmq_context.socket(zmq.REP)
        self.socket.bind('ipc:///tmp/biotracker.python.zmq')
        self.verbose = verbose

    def recv(self):
        self.msg = self.socket.recv_json()
        if self.verbose:
            print("RECV: ", self.msg)

    def send(self, msg):
        if self.verbose:
            print("SEND: ", msg)
        return self.socket.send_json(msg)

    def point_on_circle(self, a):
        r = 200
        x = math.cos(a) * r + 400
        y = math.sin(a) * r + 400
        return(x,y)


    def _inference_loop(self, ndarray):
        while True:
            self.recv()
            if self.msg['type'] != 'predict_frame':
                return
            frame_id = self.msg['frame_id']
            a = (frame_id / 10000) * 2.0 * math.pi
            x, y = self.point_on_circle(a)
            # response = self.model.predict(ndarray)
            response = [{
                'id': 0,
                'x': x,
                'y': y,
                'orientation': a # in radians
            },
            {
                'id': 0,
                'x': 700.0,
                'y': 911.0,
                'orientation': a # in radians
            }]
            self.send({
                'type': 'prediction',
                'frame_id': self.msg['frame_id'],
                'data': response,
            })
    def run(self):
        self.recv()
        while True:
            if self.msg['type'] != 'request_shared_memory':
                raise ValueError(f'Error: expected initialization message. Got: {self.msg}')
            shape = (self.msg['width'], self.msg['height'])
            with SharedArray(shape) as (shm_name, frame):
                self.send({
                    'type': 'shared_memory',
                    'path': shm_name,
                })
                self._inference_loop(frame)

class BottomUpPredictorWrapper():
    def __init__(self, model_path):
        self.predictor = load_model(model_path, batch_size=1)

    def predict(self, frame):
        try:
            frame = frame.reshape(1,frame.shape[0],frame.shape[1], 1)
            results = self.predictor.predict(frame)
            print(results.labeled_frames)
            print(results.videos)
            print(results.skeletons)
            print(results.skeletons)
            return results
        except:
            print(f"An error occured~")
#'/home/max/tmp/python_adapter/example.multi_instance'
# adapter = BiotrackerAdapter(model, verbose=True)
# adapter.run()
